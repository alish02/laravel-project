<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'        => 'required|string',
            'description' => 'required|string',
            'picture'     => 'nullable',
            'active'      => 'nullable'
        ];
    }

    public function all($keys = null)
    {
        return [
            'name'        => $this->input('name'),
            'description' => $this->input('description'),
            'picture'     => $this->file('picture'),
            'active'      => $this->input('active'),
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Это поле обязателен к заполнению',
            'mimes'    => 'Допустимые типы файлов: jpeg, jpg, png, svg',
            'unique'    => 'Категория с таким названием существует',
        ];
    }
}
