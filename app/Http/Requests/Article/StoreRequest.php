<?php

namespace App\Http\Requests\Article;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use App\Models\Article;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'        => 'required|string|unique:articles',
            'description' => 'required|string',
            'picture'     => 'required|mimes:jpeg,jpg,svg,png',
            'active'      => 'nullable',
            'category_id' => 'required'
        ];
    }

    public function all($keys = null)
    {
        return [
            'name'        => $this->input('name'),
            'description' => $this->input('description'),
            'picture'     => $this->file('picture'),
            'active'      => $this->input('active'),
            'category_id' => $this->input('category_id'),
        ];
    }

    public function messages()
    {
        return [
            'required'    => 'Это поле обязателен к заполнению',
            'mimes'       => 'Допустимые типы файлов: jpeg, jpg, png, svg',
            'unique'      => 'Категория с таким названием существует',
        ];
    }
}
