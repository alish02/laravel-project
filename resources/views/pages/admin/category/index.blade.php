@section('title', 'Админ панель | Категория')
@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6 mb-3">
                    <h3>Список категории</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Главная</a></li>
                        <li class="breadcrumb-item">Список категории</li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-9">
                    @include('components.admin.message')
                </div>
                <div class="col-sm-9 card">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Название</th>
                                <th scope="col">Активность</th>
                                <th scope="col">Автор</th>
                                <th scope="col">Действие</th>
                                <th scope="col">Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <th scope="row">{{ $category->id  }}</th>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->active ? 'Да' : 'Нет' }}</td>
                                    <td>{{ $category->user->login }}</td>
                                    <td><a class="btn btn-primary" href="{{ route('admin.category.edit', $category->id) }}">Редактировать</a></td>
                                    <td>
                                        <form action="{{ route('admin.category.destroy', $category->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger">Удалить</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
