@if (\Session::has('message'))
    <div class="alert alert-success dark alert-dismissible fade show" role="alert">
        {{\Session::get('message')}}
        <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close" data-bs-original-title="" title=""></button>
    </div>
@endif
