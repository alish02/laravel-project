import Vue from 'vue'
import router from './router/index.js'

require('./bootstrap')

const app = new Vue({
    el: '#app',
    router
})
