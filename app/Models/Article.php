<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;
    protected $guarded = false;

    protected $casts = [
        'active' => 'boolean'
    ];

    protected function active(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value,
            set: fn ($value) => ucfirst($value)
        );
    }

    public function user()
    {
        return $this->belongsTo(User::class)
            ->select(['id', 'name', 'login', 'email']);
    }

    public function category()
    {
        return $this->belongsTo(Category::class)
            ->select(['id', 'name']);
    }
}
