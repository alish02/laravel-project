<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'     => 'required',
            'login'    => 'required',
            'email'    => 'required|email',
            'password' => 'nullable'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Это поле обязателен к заполнению',
            'unique'   => 'Логин с таким названием существует',
            'email'    => 'Поле email должно быть действительным электронным адресом',
        ];
    }
}
