<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CategoryRepository
{
    public function store($data)
    {
        if (is_file($data['picture']))
            $picturePath = Storage::putFile('admin/category', $data['picture']);

        return Category::create([
            'name'        => $data['name'],
            'description' => $data['description'],
            'picture'     => $picturePath ?? NULL,
            'active'      => !empty($data['active']),
            'user_id'     => Auth::user()->getAuthIdentifier()
        ]);
    }

    public function update($data, $category)
    {
        if (is_file($data['picture']))
            $picturePath = Storage::putFile('admin/category', $data['picture']);

        return $category->update([
            'name'        => $data['name'],
            'description' => $data['description'],
            'picture'     => $picturePath ?? $category['picture'],
            'active'      => !empty($data['active']),
            'user_id'     => Auth::user()->getAuthIdentifier()
        ]);
    }

    public function getUserCategories($userId)
    {
        return Category::where('user_id', $userId)->get();
    }

    public function getActiveCategories()
    {
        return Category::all();
    }
}
