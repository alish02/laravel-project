@section('title', 'Админ панель | Авторизация')
@extends('layouts.auth')

@section('content')
    <form class="theme-form login-form" action="{{ route('auth.store') }}" method="POST">
        @include('components.admin.message')
        @csrf

        <h4 class="mb-3">Регистрация</h4>
        <div class="form-group">
            <label>Ваше имя</label>
            <div class="input-group">
                <span class="input-group-text"><i class="icon-email"></i></span>
                <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" value="{{ old('name') }}">
                @error('name')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>
        </div>
        <div class="form-group">
            <label>Login</label>
            <div class="input-group">
                <span class="input-group-text"><i class="icon-user"></i></span>
                <input class="form-control @error('login') is-invalid @enderror" type="text" name="login" value="{{ old('login') }}">
                @error('login')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>
        </div>
        <div class="form-group">
            <label>Email</label>
            <div class="input-group">
                <span class="input-group-text"><i class="icon-email"></i></span>
                <input class="form-control @error('email') is-invalid @enderror" type="text" name="email" value="{{ old('email') }}">
                @error('email')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>
        </div>
        <div class="form-group">
            <label>Пароль</label>
            <div class="input-group"><span class="input-group-text"><i class="icon-lock"></i></span>
                <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" value="{{ old('password') }}">
                @error('password')<div class="invalid-feedback">{{$message}}</div>@enderror
                <div class="show-hide"><span class="show"></span></div>
            </div>
        </div>

        <div class="form-group">
            <button class="btn btn-primary btn-block" type="submit">Зарегистрировать</button>
        </div>
    </form>
@endsection
