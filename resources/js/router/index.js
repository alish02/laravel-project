import Vue from 'vue'
import VueRouter from 'vue-router'
import AuthLogin from "../pages/admin/auth/login";
import AuthRegister from "../pages/admin/auth/register";
import AdminIndex from "../pages/admin/Index";
import CategoryIndex from "../pages/admin/category/Index";

Vue.use(VueRouter) // vueRouter

const routes = [  // список роутов
    {
        path: "/login",
        component: AuthLogin
    },
    {
        path: "/register",
        component: AuthRegister
    },
    {
        path: '/admin',
        component: AdminIndex
    },
    {
        path: "/admin/category",
        component: CategoryIndex
    }
]


export default new VueRouter({
    mode: 'history', // берет с истории
    routes // список роутов
})
