<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class RegisterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'     => 'required',
            'login'    => 'required|unique:users',
            'email'    => 'required|email',
            'password' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Это поле обязателен к заполнению',
            'unique'   => 'Логин с таким названием существует',
            'email'    => 'Поле email должно быть действительным электронным адресом',
        ];
    }
}
