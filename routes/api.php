<?php

use App\Http\Controllers\Admin\ArticleController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\IndexController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(AuthController::class)->name('auth.')->group(function () {
    Route::middleware('guest')->group(function () {
        Route::post('/login', 'login')->name('login');
//        Route::post('/login', 'check')->name('check');
        Route::get('/register', 'register')->name('register');
        Route::post('/register', 'store')->name('store');
    });
    Route::middleware('auth')->group(function () {
        Route::post('/logout', 'logout')->name('logout');
    });
});
/*
Route::middleware('auth')->prefix('admin')->name('admin.')->group(function () {
    Route::get('/', IndexController::class)->name('index');
    Route::resource('category', CategoryController::class)->except(['show']);
    Route::resource('article', ArticleController::class)->except(['show']);
    Route::resource('user', UserController::class)->except(['show']);
});*/
