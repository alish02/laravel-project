<header class="main-nav">
    <div class="sidebar-user text-center">
        <img class="img-90 rounded-circle" src="{{asset("images/admin/1.png")}}" alt="">
        <div class="badge-bottom"><span class="badge badge-primary">{{ auth()->user()->login  }}</span></div>
        <a href="{{ route('admin.index') }}">
            <h6 class="mt-3 f-14 f-w-600">{{ auth()->user()->name }}</h6>
        </a>
    </div>
    <nav>
        <div class="main-navbar">
            <div class="left-arrow" id="left-arrow">
                <i data-feather="arrow-left"></i>
            </div>
            <div id="mainnav">
                <ul class="nav-menu custom-scrollbar">
                    <li class="back-btn">
                        <div class="mobile-back text-end">
                            <span>Back</span>
                            <i class="fa fa-angle-right ps-2" aria-hidden="true"></i>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link menu-title" href="javascript:void(0)">
                            <i data-feather="users"></i><span>Пользователи</span>
                        </a>
                        <ul class="nav-submenu menu-content">
                            <li><a href="{{ route('admin.user.index') }}">Список пользователей</a></li>
                            <li><a href="{{ route('admin.user.create') }}">Создание пользователя</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link menu-title" href="javascript:void(0)">
                            <i data-feather="home"></i><span>Категория</span>
                        </a>
                        <ul class="nav-submenu menu-content">
                            <li><a href="{{route('admin.category.index')}}">Список категории</a></li>
                            <li><a href="{{route('admin.category.create')}}">Создание категории</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link menu-title" href="javascript:void(0)">
                            <i data-feather="home"></i><span>Посты</span>
                        </a>
                        <ul class="nav-submenu menu-content">
                            <li><a href="{{route('admin.article.index')}}">Список постов</a></li>
                            <li><a href="{{route('admin.article.create')}}">Создание поста</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
