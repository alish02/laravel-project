<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function checkEmailOnDB($email)
    {
        return User::where('email', $email)->exists();
    }

    public function getUserById($id)
    {
        return User::where('id', $id)
            ->select(['id', 'name', 'login', 'email'])->first();
    }

    public function getUsers()
    {
        return User::all(['id', 'name', 'login', 'email']);
    }

    public function update($data, $user)
    {
        return $user->update([
            'name'       => $data['name'] ?? $user['name'],
            'login'      => $data['login'] ?? $user['login'],
            'email'      => $data['email'] ?? $user['email'],
            'password'   => $data['password'] ?? $user['password']
        ]);
    }
}
