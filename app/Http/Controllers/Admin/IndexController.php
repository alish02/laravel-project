<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\ArticleRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class IndexController extends Controller
{
    private $userRepository;
    private $categoryRepository;
    private $articleRepository;

    public function __construct(
        UserRepository $userRepository,
        CategoryRepository $categoryRepository,
        ArticleRepository $articleRepository
    ) {
        $this->userRepository = $userRepository;
        $this->categoryRepository = $categoryRepository;
        $this->articleRepository = $articleRepository;
    }

    public function __invoke() // Контроллер одного действия, для индексной страницы
    {
        $user = $this->userRepository->getUserById(Auth::user()->getAuthIdentifier());
        $user['categories'] = $user->categories;
        $user['articles'] = $user->articles;
        $users = $this->userRepository->getUsers();

        return response()->json([
            'user' => $user,
            'users' => $users
        ], 200);
    }
}
