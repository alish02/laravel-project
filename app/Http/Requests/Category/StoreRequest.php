<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use App\Models\Category;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'        => 'required|string|unique:categories',
            'description' => 'required|string',
            'picture'     => 'required|mimes:jpeg,jpg,svg,png',
            'active'      => 'nullable'
        ];
    }

    public function all($keys = null)
    {
        return [
            'name'        => $this->input('name'),
            'description' => $this->input('description'),
            'picture'     => $this->file('picture'),
            'active'      => $this->input('active')
        ];
    }

    public function messages()
    {
        return [
            'required'    => 'Это поле обязателен к заполнению',
            'mimes'       => 'Допустимые типы файлов: jpeg, jpg, png, svg',
            'unique'      => 'Категория с таким названием существует',
        ];
    }
}
