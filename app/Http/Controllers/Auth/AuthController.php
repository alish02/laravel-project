<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Repositories\UserRepository;

class AuthController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function login(LoginRequest $request)
    {
        if (Auth::check()) {
            return to_route('admin.index');
        }

        $validator = $request->validated();
        if (Auth::attempt($validator)) {
            return response()->json([
                'success' => true
            ], 200);
        };

        return response()->json([
            'success' => false,
            'message' => 'Пользователь не найден'
        ], 404);
    }

    public function register()
    {
        return view('pages.auth.register');
    }

    public function store(RegisterRequest $request)
    {
        if (Auth::check())
            return to_route('admin.index');
        $validator = $request->validated();

        if ($this->userRepository->checkEmailOnDB($validator['email'])) {
            return to_route('auth.register')
                ->with(['message' => 'Пользователь с такой почтой существует'])->withInput();
        }
        $user = User::create($validator);
        if ($user) {
            Auth::login($user);
            return to_route('admin.index');
        }

        return to_route('auth.login')->with(['message' => 'Произошла ошибка']);
    }

    public function logout()
    {
        Auth::logout();
        return to_route('auth.login');
    }

    public function username()
    {
        return 'login';
    }
}
