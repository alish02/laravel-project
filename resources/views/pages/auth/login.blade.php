@section('title', 'Админ панель | Авторизация')
@extends('layouts.auth')

@section('content')
    <form class="theme-form login-form" action="{{ route('auth.check') }}" method="POST">
        @csrf
        <h4 class="mb-3">Авторизация</h4>

        @include('components.admin.message')

        <div class="form-group">
            <label>Login</label>
            <div class="input-group">
                <span class="input-group-text"><i class="icon-email"></i></span>
                <input class="form-control @error('name') is-login @enderror" type="text" name="login" value="{{ old('login') }}">
                @error('login')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>
        </div>
        <div class="form-group">
            <label>Пароль</label>
            <div class="input-group"><span class="input-group-text"><i class="icon-lock"></i></span>
                <input class="form-control @error('password') is-login @enderror" type="password" name="password" value="{{ old('password') }}">
                @error('password')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>
        </div>

        <div class="form-group">
            <button class="btn btn-primary btn-block" type="submit">Вход</button>
        </div>
        <p>У вас нету аккаунта? <a href="{{ route('auth.register') }}">Рагистрирусйтесь</a></p>
    </form>
@endsection
