<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Repositories\UserRepository;
use App\Models\User;

class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        return 'index';
    }

    public function list()
    {
        $users = $this->userRepository->getUsers();
        return view('pages.admin.user.list', compact('users'));
    }

    public function create()
    {
        return view('pages.admin.user.create');
    }

    public function store(RegisterRequest $request)
    {
        $validator = $request->validated();

        if ($this->userRepository->checkEmailOnDB($validator['email'])) {
            return to_route('admin.user.create')
                ->with(['message' => 'Пользователь с такой почтой существует'])->withInput();
        }

        $user = User::create($validator);
        if ($user) {
            return to_route('admin.user.list')
                ->with(['message' => 'Пользователь успешно создан']);
        }

        return to_route('auth.login')->with(['message' => 'Произошла ошибка']);
    }
}
