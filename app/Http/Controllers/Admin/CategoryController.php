<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
//        $categories = $this->categoryRepository->getUserCategories(Auth::user()->getAuthIdentifier());
        $categories = $this->categoryRepository->getActiveCategories();

        return response()->json([
            'success' => true,
            'categories' => $categories
        ], 200);
    }

    public function create()
    {
        return view('pages.admin.category.create');
    }

    public function store(StoreRequest $request)
    {
        $validator = $request->validated();
        $this->categoryRepository->store($validator);

        return to_route('admin.category.index')
            ->with('message', 'Категория успешно добавлена');
    }

    public function edit(Category $category)
    {
        return view('pages.admin.category.edit', compact('category'));
    }

    public function update(UpdateRequest $request, Category $category)
    {
        $validator = $request->validated();
        $this->categoryRepository->update($validator, $category);

        return to_route('admin.category.index')
            ->with('message', 'Категория успешно обновлена');
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return to_route('admin.category.index')
            ->with('message', 'Категория успешно удалена');
    }
}
