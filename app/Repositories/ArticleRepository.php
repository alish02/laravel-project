<?php

namespace App\Repositories;

use App\Models\Article;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ArticleRepository
{
    public function store($data)
    {
        if (is_file($data['picture']))
            $picturePath = Storage::putFile('admin/article', $data['picture']);

        return Article::create([
            'name'        => $data['name'],
            'description' => $data['description'],
            'picture'     => $picturePath ?? NULL,
            'active'      => !empty($data['active']),
            'user_id'     => Auth::user()->getAuthIdentifier(),
            'category_id' => $data['category_id']
        ]);
    }

    public function update($data, $article)
    {
        if (is_file($data['picture']))
            $picturePath = Storage::putFile('admin/article', $data['picture']);

        return $article->update([
            'name'        => $data['name'],
            'description' => $data['description'],
            'picture'     => $picturePath ?? $article['picture'],
            'active'      => !empty($data['active']),
            'user_id'     => Auth::user()->getAuthIdentifier(),
            'category_id' => $data['category_id']
        ]);
    }

    public function getUserArticles($userId)
    {
        return Article::where('user_id', $userId)->get();
    }
}
