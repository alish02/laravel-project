@section('title', 'Админ панель | Категория')
@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6 mb-3">
                    <h3>Пользователи</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Главная</a></li>
                        <li class="breadcrumb-item">Список пользователей</li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-9">
                    @include('components.admin.message')
                </div>
                <div class="col-sm-9 card">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Имя</th>
                                <th scope="col">Логин</th>
                                <th scope="col">Email</th>
                                <th scope="col">Категории</th>
                                <th scope="col">Посты</th>
                                <th scope="col">Действие</th>
                                <th scope="col">Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <th scope="row">{{ $user->id }}</th>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->login }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ count($user->categories) }}</td>
                                    <td>{{ count($user->categories) }}</td>
                                    <td><a class="btn btn-primary {{$user->id != auth()->user()->id ? 'disabled' : ''}}" href="{{ route('admin.user.edit', $user->id ) }}">Редактировать</a></td>
                                    <td>
                                        <form action="{{ route('admin.user.destroy', $user->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger {{auth()->user()->login == 'admin' ? '' : 'disabled'}}">Удалить</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
