<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\UpdateRequest;
use App\Repositories\UserRepository;
use App\Models\User;

class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $users = $this->userRepository->getUsers();
        return view('pages.admin.user.index', compact('users'));
    }

    public function create()
    {
        return view('pages.admin.user.create');
    }

    public function store(RegisterRequest $request)
    {
        $validator = $request->validated();

        if ($this->userRepository->checkEmailOnDB($validator['email'])) {
            return to_route('admin.user.create')
                ->with(['message' => 'Пользователь с такой почтой существует'])->withInput();
        }

        $user = User::create($validator);
        if ($user) {
            return to_route('admin.user.index')
                ->with(['message' => 'Пользователь успешно создан']);
        }

        return to_route('auth.login')->with(['message' => 'Произошла ошибка']);
    }

    public function show($id)
    {
        //
    }

    public function edit(User $user)
    {
        return view('pages.admin.user.edit', compact('user'));
    }

    public function update(UpdateRequest $request, User $user)
    {
        $validator = $request->validated();
        $this->userRepository->update($validator, $user);

        return to_route('admin.user.index')
            ->with('message', 'Пользователь успешно обновлен');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return to_route('admin.user.index')
            ->with('message', 'Пользователь успешно удален');
    }
}
