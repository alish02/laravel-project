@section('title', 'Админ панель | Категория')
@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6 mb-3">
                    <h3>Список постов</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Главная</a></li>
                        <li class="breadcrumb-item">Список постов</li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-9">
                    @include('components.admin.message')
                </div>
                <div class="col-sm-9 card">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Название</th>
                                <th scope="col">Активность</th>
                                <th scope="col">Автор</th>
                                <th scope="col">Категория</th>
                                <th scope="col">Действие</th>
                                <th scope="col">Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($articles as $article)
                                <tr>
                                    <th scope="row">{{ $article->id  }}</th>
                                    <td>{{ $article->name }}</td>
                                    <td>{{ $article->active ? 'Да' : 'Нет' }}</td>
                                    <td>{{ $article->user->login }}</td>
                                    <td>{{ $article->category->name }}</td>
                                    <td><a class="btn btn-primary" href="{{ route('admin.article.edit', $article->id) }}">Редактировать</a></td>
                                    <td>
                                        <form action="{{ route('admin.article.destroy', $article->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger">Удалить</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
