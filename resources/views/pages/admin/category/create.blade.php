@section('title', 'Админ панель | Создание категории')
@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6">
                    <h3>Создание категории</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Главная</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.category.index')}}">Список категории</a></li>
                        <li class="breadcrumb-item">Создание категории</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-8">
        <form class="card" action="{{route('admin.category.store')}}" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="card-body">
                <div class="theme-form">
                    <div class="mb-3">
                        <label class="col-form-label">Название</label>
                        <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" value="{{ old('name') }}">
                        @error('name')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Описание</label>
                        <textarea class="form-control @error('description') is-invalid @enderror" name="description" rows="3">{{ old('description') }}</textarea>
                        @error('description')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <div class="mb-3">
                        <input class="form-control @error('picture') is-invalid @enderror" type="file" name="picture">
                        @error('picture')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <div class="mb-3">
                        <label class="d-block" for="chk-ani">
                            <input class="checkbox_animated" id="chk-ani" name="active" type="checkbox" checked>Активность
                        </label>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary" type="submit">Добавить</button>
                <a href="{{route('admin.category.index')}}" class="btn btn-secondary">Отмена</a>
            </div>
        </form>
    </div>
@endsection
