<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Repositories\ArticleRepository;
use App\Http\Requests\Article\StoreRequest;
use App\Http\Requests\Article\UpdateRequest;
use App\Repositories\CategoryRepository;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    private $articleRepository;
    private $categoryRepository;

    public function __construct(
        ArticleRepository $articleRepository,
        CategoryRepository $categoryRepository
    ) {
        $this->articleRepository = $articleRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $articles = $this->articleRepository->getUserArticles(Auth::user()->getAuthIdentifier());
        return view('pages.admin.article.index', compact('articles'));
    }

    public function create()
    {
        $categories = $this->categoryRepository->getActiveCategories();
        return view('pages.admin.article.create', compact('categories'));
    }

    public function store(StoreRequest $request)
    {
        $validator = $request->validated();
        $this->articleRepository->store($validator);

        return to_route('admin.article.index')
            ->with('message', 'Пост успешно добавлен');
    }

    public function edit(Article $article)
    {
        $categories = $this->categoryRepository->getActiveCategories();
        return view('pages.admin.article.edit', compact('article'), compact('categories'));
    }

    public function update(UpdateRequest $request, Article $article)
    {
        $validator = $request->validated();
        $this->articleRepository->update($validator, $article);

        return to_route('admin.article.index')
            ->with('message', 'Пост успешно обновлен');
    }

    public function destroy(Article $article)
    {
        $article->delete();
        return to_route('admin.article.index')
            ->with('message', 'Пост успешно удален');
    }
}
