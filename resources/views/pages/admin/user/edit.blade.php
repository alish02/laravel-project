@section('title', 'Админ панель | Категория')
@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row mb-4">
                <div class="col-sm-6 mb-3">
                    <h3>Пользователи</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Главная</a></li>
                        <li class="breadcrumb-item">Редатирование пользователя</li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <form class="col-sm-5" action="{{ route('admin.user.update', $user->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    @include('components.admin.message')

                    <div class="form-group">
                        <label>Имя</label>
                        <div class="input-group">
                            <span class="input-group-text"><i class="icon-email"></i></span>
                            <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" value="{{ old('name', $user->name) }}">
                            @error('name')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Login</label>
                        <div class="input-group">
                            <span class="input-group-text"><i class="icon-user"></i></span>
                            <input class="form-control @error('login') is-invalid @enderror" type="text" name="login" value="{{ old('login', $user->login) }}">
                            @error('login')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <div class="input-group">
                            <span class="input-group-text"><i class="icon-email"></i></span>
                            <input class="form-control @error('email') is-invalid @enderror" type="text" name="email" value="{{ old('email', $user->email) }}">
                            @error('email')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Пароль</label>
                        <div class="input-group"><span class="input-group-text"><i class="icon-lock"></i></span>
                            <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" value="{{ old('password') }}">
                            @error('password')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary btn-block" type="submit">Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
