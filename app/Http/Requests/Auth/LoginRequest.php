<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'login'    => 'required',
            'password' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Это поле обязателен к заполнению',
            'unique'   => 'Логин с таким названием существует',
        ];
    }
}
